#ifndef MUXFORWARDMOD_H
#define MUXFORWARDMOD_H

/**
 *
 * 4:1 Mux module template.
 */

#include <systemc.h>

/**
 * Mux module.
 * Mux module models a generic 4:1 multiplexer of template type T. 
 * Implementation based on a template class.
 *
 */

template <class T> class muxForward: public sc_module
{
public:
  sc_in< T >  din0;
  sc_in< T >  din1;   
  sc_in< T >  din2;
  sc_in< T >  din3;
  sc_in< sc_uint<2> > sel;             
  sc_out< T > dout;

  SC_CTOR(muxForward)
     {      
      SC_METHOD(entry);
      sensitive << din0 << din1 << din2 << din3 << sel;
    }
  
  void entry();
};


template <class T> void muxForward<T>::entry()
{
    switch(sel.read())
    {
		case 0:
			dout.write(din0.read());
			break;
		case 1:
			dout.write(din1.read());
			break;
		case 2:
			dout.write(din2.read());
			break;
		case 3:
			dout.write(din3.read());
	}
}

#endif
