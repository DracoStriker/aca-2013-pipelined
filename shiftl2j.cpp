
#include "shiftl2j.h"

/**
 * \c shifl2 module callback function.
 */
void shiftl2j::entry()
{
    unsigned int pc = din0.read();
    unsigned int inst = din1.read();
    pc &= 0xF000;
    inst &= 0x03FF;
    inst <<= 2;
    dout.write(pc | inst);
}
