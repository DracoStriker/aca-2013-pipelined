
#include "hazard.h"

/**
 * Callback for the hazrd detection of \c hazard module.
 */
void hazard::detect_hazard()
{
	if(     rs.read()!=0 && (branch.read() || jr.read()) && rs.read()==WriteReg_exe.read() && RegWrite_exe.read()
		||  rt.read()!=0 && (branch.read() || jr.read()) && rt.read()==WriteReg_exe.read() && RegWrite_exe.read() 
		||  rs.read()!=0 && MemRead_exe.read()   && rs.read()==WriteReg_exe.read()
		||  rt.read()!=0 && MemRead_exe.read()   && rt.read()==WriteReg_exe.read() && !MemRead_id.read()
		||  rs.read()!=0 && MemRead_mem.read()   && rs.read()==WriteReg_mem.read()
		||  rt.read()!=0 && MemRead_mem.read()   && rt.read()==WriteReg_mem.read() && !MemRead_id.read() && !MemWrite_id.read() 
		||  rt.read()!=0 && MemRead_mem2.read()  && (branch.read() || jr.read()) && rt.read()==WriteReg_mem2.read()
		||  rs.read()!=0 && MemRead_mem2.read()  && (branch.read() || jr.read()) && rs.read()==WriteReg_mem2.read())
		{
			enable_pc.write(false);
			enable_ifid.write(false);
			reset_ifid.write(false);
			reset_idexe.write(true);
			reset_exemem1.write(false);
		}
		else if(BranchTaken.read() || j.read() || jr.read())
		{
			enable_pc.write(true);
			enable_ifid.write(true);
			reset_ifid.write(true);
			reset_idexe.write(false);
			reset_exemem1.write(false);
		}
		else
		{
			enable_pc.write(true);
			enable_ifid.write(true);
			reset_ifid.write(false);
			reset_idexe.write(false);
			reset_exemem1.write(false);
		}
}

