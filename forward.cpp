
#include "forward.h"

/**
 * Callback for the forward detection of \c forward module.
 */
void forward::detect_forward()
{
	//data forwards (ID)
	
	if (RegWrite_mem1.read() && (rs_id.read() == WriteReg_mem1.read())) mux1_id.write(3);
	
	else if (RegWrite_mem2.read() && (rs_id.read() == WriteReg_mem2.read())) mux1_id.write(2);
	
	else if (RegWrite_wb.read() && (rs_id.read() == WriteReg_wb.read())) mux1_id.write(1);
	
	else mux1_id.write(0);
	
	
	if (RegWrite_mem1.read() && (rt_id.read() == WriteReg_mem1.read())) mux2_id.write(3);
	
	else if (RegWrite_mem2.read() && (rt_id.read() == WriteReg_mem2.read())) mux2_id.write(2);
	
	else if (RegWrite_wb.read() && (rt_id.read() == WriteReg_wb.read())) mux2_id.write(1);
	
	else mux2_id.write(0);
	
	
	//data forwards (EXE)
	
	if (RegWrite_mem1.read() && (rs_exe.read() == WriteReg_mem1.read())) mux1_exe.write(3);
	
	else if (RegWrite_mem2.read() && (rs_exe.read() == WriteReg_mem2.read())) mux1_exe.write(2);
	
	else if (RegWrite_wb.read()	&& (rs_exe.read() == WriteReg_wb.read())) mux1_exe.write(1);
	
	else mux1_exe.write(0);
	
	
	if (RegWrite_mem1.read() && (rt_exe.read() == WriteReg_mem1.read())) mux2_exe.write(3);
	
	else if (RegWrite_mem2.read() && (rt_exe.read() == WriteReg_mem2.read())) mux2_exe.write(2);
	
	else if (RegWrite_wb.read() && (rt_exe.read() == WriteReg_wb.read())) mux2_exe.write(1);
	
	else mux2_exe.write(0);
	
	
	//data forwards (MEM1)
	
	if (MemWrite_mem1.read() && RegWrite_mem2.read() && (rt_mem1.read() == WriteReg_mem2.read())) mux1_mem1.write(3);
	
	else if (MemWrite_mem1.read() && RegWrite_wb.read() && (rt_mem1.read() == WriteReg_wb.read())) mux1_mem1.write(2);
	
	else if (MemWrite_mem1.read() && RegWrite_aux.read() && (rt_mem1.read() == WriteReg_aux.read())) mux1_mem1.write(1);
	
	else mux1_mem1.write(0);
}
