//MIPSarch.cpp
// 
// Widget to display MIPS architecture
//

#include <qpixmap.h>
#include <qevent.h>
#include <qcursor.h>

#include "PortValItem.h"
#include "PortValRead.h"

#include "MIPSarch.h"
#include "../mips.h"
#include "../mipsaux.h"

/**
 * MIPSarchcanvas constructor.
 */

MIPSarchCanvas::MIPSarchCanvas(mips &m, QObject *parent) : QCanvas(parent), mips1(m)
{
    // set background to MipsArch.png
    QPixmap backFig("GUI/MIPS_datapathPipe4.png");
    resize(backFig.width(),backFig.height());
    setBackgroundPixmap(backFig);

    // create and position the items that display the port values
    PortValItem *portVal;
    QCanvasText *qText;

    // instruction labels in stages
    qText=new QCanvasText("nop",this);
    qText->setVisible(true);
    qText->move(10,10);
    qText->setColor(QColor("red"));
    instStage.push_back(qText);
    pcStage.push_back(new PortValRead(mips1.PCreg->dout,"PC"));
    validpcStage.push_back(new PortValRead(mips1.reg_if_id->valid_if,"valid_PC"));

    qText=new QCanvasText("nop",this);
    qText->setVisible(true);
    qText->move(253,10);
    qText->setColor(QColor("red"));
    instStage.push_back(qText);
    pcStage.push_back(new PortValRead(mips1.reg_if_id->PC_id,"PC_id"));
    validpcStage.push_back(new PortValRead(mips1.reg_if_id->valid_id,"valid_PC_id"));

    qText=new QCanvasText("nop",this);
    qText->setVisible(true);
    qText->move(484,10);
    qText->setColor(QColor("red"));
    instStage.push_back(qText);
    pcStage.push_back(new PortValRead(mips1.reg_id_exe->PC_exe,"PC_exe"));
    validpcStage.push_back(new PortValRead(mips1.reg_id_exe->valid_exe,"valid_PC_exe"));
    
    qText=new QCanvasText("nop",this);
    qText->setVisible(true);
    qText->move(661,10);
    qText->setColor(QColor("red"));
    instStage.push_back(qText);
    pcStage.push_back(new PortValRead(mips1.reg_exe_mem->PC_mem,"PC_mem"));
    validpcStage.push_back(new PortValRead(mips1.reg_exe_mem->valid_mem,"valid_PC_mem"));
    
    qText=new QCanvasText("nop",this);
    qText->setVisible(true);
    qText->move(741,30);
    qText->setColor(QColor("red"));
    instStage.push_back(qText);
    pcStage.push_back(new PortValRead(mips1.reg_mem1_mem2->PC_mem2,"PC_mem2"));
    validpcStage.push_back(new PortValRead(mips1.reg_mem1_mem2->valid_mem2,"valid_PC_mem2"));

    qText=new QCanvasText("nop",this);
    qText->setVisible(true);
    qText->move(816,10);
    qText->setColor(QColor("red"));
    instStage.push_back(qText);
    pcStage.push_back(new PortValRead(mips1.reg_mem_wb->PC_wb,"PC_wb"));
    validpcStage.push_back(new PortValRead(mips1.reg_mem_wb->valid_wb,"valid_PC_wb"));

    // value of port signals
    // IF
    portVal=new PortValItem(this,mips1.instmem->addr, "PC");
    portVal->move(81,260);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.add4->res, "PC4");
    portVal->move(110,91);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.PCreg->din, "NPC");
    portVal->move(11,244);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.instmem->inst, "inst");
    portVal->move(153,261);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    //ID
    portVal=new PortValItem(this,mips1.ctrl->RegWrite, "RegWrite");
    portVal->move(383,54);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.ctrl->MemtoReg, "MemtoReg");
    portVal->move(383,66);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.ctrl->Branch, "Branch");
    portVal->move(383,78);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.ctrl->MemRead, "MemRead");
    portVal->move(383,90);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.ctrl->MemWrite, "MemWrite");
    portVal->move(383,102);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.ctrl->ALUSrc, "ALUSrc");
    portVal->move(383,114);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.ctrl->ALUOp, "ALUOp");
    portVal->move(383,126);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.ctrl->RegDst, "RegDst");
    portVal->move(383,138);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_if_id->inst_id, "inst_id");
    portVal->move(218,261);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.dec1->opcode, "opcode");
    portVal->move(284,100);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.dec1->funct, "funct");
    portVal->move(302,135);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);


    portVal=new PortValItem(this,mips1.dec1->rd, "rd");
    portVal->move(365,364);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.dec1->imm, "imm");
    portVal->move(299,388);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.rfile->reg1, "r1");
    portVal->move(334,224);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.rfile->reg2, "r2");
    portVal->move(334,243);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.rfile->data1, "r1_out");
    portVal->move(422,226);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.rfile->data2, "r2_out");
    portVal->move(422,255);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.e1->dout, "imm_ext");
    portVal->move(403,398);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.hazard_unit->enable_pc, "enable_pc");
    portVal->move(124,412);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.hazard_unit->reset_idexe, "reset_haz_idexe");
    portVal->move(280,422);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    //EXE
    portVal=new PortValItem(this,mips1.reg_id_exe->RegWrite_exe, "RegWrite_exe");
    portVal->move(475,54);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->MemtoReg_exe, "MemtoReg_exe");
    portVal->move(475,66);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->Branch_exe, "Branch_exe");
    portVal->move(475,78);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->MemRead_exe, "MemRead_exe");
    portVal->move(475,90);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->MemWrite_exe, "MemWrite_exe");
    portVal->move(475,102);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->ALUSrc_exe, "ALUSrc_exe");
    portVal->move(475,114);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->ALUOp_exe, "ALUOp_exe");
    portVal->move(475,126);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->rega_exe, "rega_exe");
    portVal->move(472,222);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->regb_exe, "regb_exe");
    portVal->move(472,254);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.alu1->din2, "alu_din2");
    portVal->move(535,287);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->WriteReg_exe, "WriteReg_exe");
    portVal->move(472,353);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_id_exe->PC4_exe, "PC4_exe");
    portVal->move(472,171);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.addbr->res, "BranchTarget");
    portVal->move(599,171);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.sl2->dout, "addr_ext");
    portVal->move(519,192);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.alu1->dout, "alu_dout");
    portVal->move(591,269);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.alu1->zero, "zero");
    portVal->move(591,236);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);


    //MEM
    portVal=new PortValItem(this,mips1.reg_exe_mem->RegWrite_mem, "RegWrite_mem");
    portVal->move(652,54);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->MemtoReg_mem, "MemtoReg_mem");
    portVal->move(652,66);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->Branch_mem, "Branch_mem");
    portVal->move(652,78);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->MemRead_mem, "MemRead_mem");
    portVal->move(652,90);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->MemWrite_mem, "MemWrite_mem");
    portVal->move(652,102);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->aluOut_mem, "aluOut_mem");
    portVal->move(652,253);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->regb_mem, "regb_mem");
    portVal->move(684,297);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->WriteReg_mem, "WriteReg_mem");
    portVal->move(652,349);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.datamem->dout, "dmem.dout");
    portVal->move(758,303);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->Zero_mem, "Zero_mem");
    portVal->move(663,205);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.a1->dout, "BranchTaken");
    portVal->move(449,7);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_exe_mem->BranchTarget_mem, "BranchTarget_mem");
    portVal->move(465,26);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

	//MEM2

    portVal=new PortValItem(this,mips1.reg_mem1_mem2->aluOut_mem2, "aluOut_mem2"); //
    portVal->move(776,335);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_mem1_mem2->MemtoReg_mem2, "MemtoReg_mem2"); //
    portVal->move(776,66);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_mem1_mem2->WriteReg_mem2, "WriteReg_mem2"); //
    portVal->move(776,349);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_mem1_mem2->RegWrite_mem2, "RegWrite_mem2"); //
    portVal->move(776,54);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    //WB
    
    portVal=new PortValItem(this,mips1.reg_mem_wb->memOut_wb, "memOut_wb");
    portVal->move(806,301);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_mem_wb->aluOut_wb, "aluOut_wb");
    portVal->move(806,328);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_mem_wb->MemtoReg_wb, "MemtoReg_wb");
    portVal->move(806,63);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_mem_wb->WriteReg_wb, "WriteReg_wb");
    portVal->move(612,417);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.reg_mem_wb->RegWrite_wb, "RegWrite_wb");
    portVal->move(612,395);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);

    portVal=new PortValItem(this,mips1.rfile->datawr, "WriteVal");
    portVal->move(612,430);
    portVal->setColor(QColor("blue"));
    portValVec.push_back(portVal);



    updateArch();

    setDoubleBuffering(true);
}

/**
 * updates the values of all ports values.
 */
void MIPSarchCanvas::updateArch(void)
{
    unsigned int p,s,addr;
    char instText[200];
    for(p=0; p<portValVec.size(); p++) {
         portValVec[p]->updateVal();
    }
    for(s=0; s<instStage.size(); s++) {
	 if(validpcStage[s]->read() == 1) { // Stage PC is valid
	     if(pcStage[s]->read() < mips1.instmem->size()) { // StagePC is less than size of instmem
	        addr=mips1.instmem->at(pcStage[s]->read());
	        disassemble(addr,instText);
                instStage[s]->setText(instText);
	     }
	     else instStage[s]->setText("nop");
	 }
	 else instStage[s]->setText("bubble");
    }
    update();
}

MIPSarchCanvas::~MIPSarchCanvas()
{
    // no need to delete child widgets, Qt does it all for us
    for(unsigned int p=0; p < portValVec.size(); p++) {
       delete portValVec[p];
       portValVec[p]=0;
    }
}


/**
 * MIPSarch contructor.
 * Creates and sets the MIPSarchcanvas to be displayed, and some
 * GUI functionalities.
 */
MIPSarch::MIPSarch( mips &m, QWidget* parent,  const char* name, WFlags fl )
    : QCanvasView(0, parent, name, fl )
{   
    archCanvas=new MIPSarchCanvas(m,this);
    setCanvas(archCanvas);

    setIcon(QPixmap("mips.xpm"));

    resize(QSize(896+5,455+5));
    setMaximumSize(QSize(896+5,455+5));

    cursor=new QCursor(PointingHandCursor);
    setCursor(*cursor);

    viewport()->setMouseTracking(true);
    setFocusPolicy(QWidget::StrongFocus);
}

/*  
 *  Destroys the object and frees any allocated resources
 */
MIPSarch::~MIPSarch()
{
    // no need to delete child widgets, Qt does it all for us
    delete cursor;
}

/**
 * updates the port values.
 */
void MIPSarch::updateArch(void)
{
     archCanvas->updateArch();
}

/**
 * emits signals when click is inside certain regions
 */
void MIPSarch::contentsMousePressEvent(QMouseEvent *e)
{
/*

    QPoint pos=e->pos();
    if(pos.x() > 80 && pos.x() < 112 
       && pos.y()> 200 && pos.y() < 270)
       emit imemClicked();
    if(pos.x() > 320 && pos.x() < 370 
       && pos.y()> 190 && pos.y() < 300)
       emit regfileClicked();
    if(pos.x() > 590 && pos.x() < 645 
       && pos.y()> 220 && pos.y() < 280)
       emit dmemClicked();
*/
}

/**
 * changes cursor when mouse is over certain regions
 */

void MIPSarch::contentsMouseMoveEvent(QMouseEvent *e)
{
    QPoint pos=e->pos();

//    fprintf(stderr,"x=%4d y=%4d\n",pos.x(),pos.y());

    cursor->setShape(ArrowCursor);
    setCursor(*cursor);

}

/**
 * This window is not to be closed.
 */
void MIPSarch::closeEvent(QCloseEvent *e)
{
   e->ignore();
}
