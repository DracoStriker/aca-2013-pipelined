#ifndef FORWARDMOD_H
#define FORWARDMOD_H

/**
 *
 * forward module interface.
 */

#include <systemc.h>

/**
 * forward module.
 * forward module is the forward detection unit.
 *
 *   - input ports
 *   	- \c sc_uint<5> \c rs_id	   - first register being read at ID phase
 *   	- \c sc_uint<5> \c rt_id	   - second register being read at ID phase
 *   	- \c sc_uint<5> \c rs_exe	   - first register being read at EXE phase
 *   	- \c sc_uint<5> \c rt_exe	   - second register being read at EXE phase
 *   	- \c sc_uint<5> \c rt_mem1	   - first register being read at MEM1 phase
 *   	- \c sc_uint<5> \c WriteReg_mem1	   - register to be written (MEM1)
 *   	- \c sc_uint<5> \c WriteReg_mem2	   - register to be written (MEM2)
 *   	- \c sc_uint<5> \c WriteReg_wb	   - register to be written (WB)
 *   	- \c sc_uint<5> \c WriteReg_aux	   - register to be written (AUX)
 *   	- \c bool \c RegWrite_mem1	   - control signal of writing registers (MEM1)
 *   	- \c bool \c RegWrite_mem2	   - control signal of writing registers (MEM2)
 *   	- \c bool \c RegWrite_wb	   - control signal of writing registers (WB)
 *   	- \c bool \c RegWrite_aux	   - control signal of writing registers (AUX)
 *   	- \c bool \c MemWrite_mem1	   - control signal of writing data memory (MEM1)
 *   - output ports
 *   	- \c bool \c mux1_id      - selects forwarding mux 1 input (ID)
 *   	- \c bool \c mux2_id    - selects forwarding mux 2 input (ID)
 *   	- \c bool \c mux1_exe    - selects forwarding mux 1 input (EXE)
 *   	- \c bool \c mux1_exe    - selects forwarding mux 2 input (EXE)
 *   	- \c bool \c mux1_mem1    - selects forwarding mux 1 input (MEM1)
 */

SC_MODULE( forward )
{
  public: 
    sc_in< sc_uint<5> >  rs_id, rs_exe;
    sc_in< sc_uint<5> >  rt_id, rt_exe, rt_mem1;        
    sc_in< sc_uint<5> >  WriteReg_mem1, WriteReg_mem2, WriteReg_wb, WriteReg_aux;        
    sc_in< bool >  RegWrite_mem1, RegWrite_mem2, RegWrite_wb, RegWrite_aux;
    sc_in< bool >  MemWrite_mem1;      
    
    sc_out< sc_uint<2> >  mux1_id, mux2_id, mux1_exe, mux2_exe, mux1_mem1;     

    SC_CTOR(forward)
    {      
        SC_METHOD(detect_forward);
        sensitive << rs_id << rs_exe
          << rt_id << rt_exe << rt_mem1
		  << WriteReg_mem1 << WriteReg_mem2 << WriteReg_wb << WriteReg_aux
		  << RegWrite_mem1 << RegWrite_mem2 << RegWrite_wb << RegWrite_aux
		  << MemWrite_mem1;
    }
  
    void detect_forward();
};

#endif
