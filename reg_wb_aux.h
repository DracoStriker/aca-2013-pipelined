#ifndef REG_WB_AUX_H
#define REG_WB_AUX_H

/**
 *
 * reg_wb_aux_t module interface.
 */

#include <systemc.h>

#include "regT.h"

/**
 * reg_wb_aux_t module.
 * reg_wb_aux_t module is the WB/AUX pipeline register. 
 */

SC_MODULE(reg_wb_aux_t) {

	// Ports
	
	sc_in  < bool > clk;
	sc_in  < bool > reset;
	sc_in  < bool > enable;

	sc_in  < sc_uint<32> > WriteVal_wb;
	sc_out < sc_uint<32> > WriteVal_aux;

	sc_in  < sc_uint<5> > WriteReg_wb;
	sc_out < sc_uint<5> > WriteReg_aux;

	sc_in  < bool > RegWrite_wb;
	sc_out < bool > RegWrite_aux;
	
	sc_in  < sc_uint<32> > PC_wb;   // only for visualization purposes
	sc_out < sc_uint<32> > PC_aux;   // only for visualization purposes
	sc_in  < bool > valid_wb;       // only for visualization purposes
	sc_out < bool > valid_aux;       // only for visualization purposes

	// Modules

	regT < sc_uint<32> > *WriteVal;
	regT < sc_uint<5> > *WriteReg;
	regT < bool > *RegWrite;
	
	regT < sc_uint<32> > *PC;        // only for visualization purposes
	regT < bool > *valid;            // only for visualization purposes

	SC_CTOR(reg_wb_aux_t) {
		
		WriteVal = new regT < sc_uint<32> > ("WriteVal");;
		WriteVal->din(WriteVal_wb);
		WriteVal->dout(WriteVal_aux);
		WriteVal->clk(clk);
		WriteVal->enable(enable);
		WriteVal->reset(reset);

		WriteReg = new regT < sc_uint<5> > ("WriteReg");;
		WriteReg->din(WriteReg_wb);
		WriteReg->dout(WriteReg_aux);
		WriteReg->clk(clk);
		WriteReg->enable(enable);
		WriteReg->reset(reset);

		RegWrite = new regT < bool >("RegWrite");
		RegWrite->din(RegWrite_wb);
		RegWrite->dout(RegWrite_aux);
		RegWrite->clk(clk);
		RegWrite->enable(enable);
		RegWrite->reset(reset);

		PC = new regT < sc_uint<32> > ("PC");;
		PC->din(PC_wb);
		PC->dout(PC_aux);
		PC->clk(clk);
		PC->enable(enable);
		PC->reset(reset);

		valid = new regT < bool > ("valid");;
		valid->din(valid_wb);
		valid->dout(valid_aux);
		valid->clk(clk);
		valid->enable(enable);
		valid->reset(reset);
	}
};

#endif

