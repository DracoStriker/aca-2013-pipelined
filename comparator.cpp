
// comparator.cpp

#include "comparator.h"

/**
 * callback function of module \c comparator.
 */
void comparator::calc()
{
   res.write(op1.read() == op2.read());
}

