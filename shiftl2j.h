#ifndef SHIFTL2JMOD_H
#define SHIFTL2JMOD_H

/**
 *
 * \c shiftl2 for jump offset module interface.
 */

#include <systemc.h>

/**
 * \c shiftl2 for jump offset module.
 * \c shiftl2 for jump offset module shifts a sc_uint<32> two bits to the left and concatenates the inputs.
 *   - input ports
 *   	- \c sc_uint<32> \c din0	- input
 *   	- \c sc_uint<32> \c din1	- input
 *   - output ports
 *   	- \c sc_uint<32> \c dout	- output
 */

SC_MODULE(shiftl2j) {
  
  sc_in < sc_uint<32> >  din0;
  sc_in < sc_uint<32> >  din1;
  sc_out< sc_uint<32> >  dout;

  SC_CTOR(shiftl2j)
     {      
      SC_METHOD(entry);
      sensitive << din0 << din1;
    }
  
  void entry();
};

#endif
